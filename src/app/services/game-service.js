angular.module('app')
.service('gameService', function($http) {

    function saveResult(result) {
        return $http.post("http://localhost:8081/api/result/", result)
    }

    function saveResultOrthography(resultOrthography) {
        return $http.post("http://localhost:8081/api/resultOrthography/", resultOrthography)
    }

    function getOrthography() {
        return $http.get("http://localhost:8081/api/orthography/")
    }

    function getWords() {
        return $http.get("http://localhost:8081/api/words/")
    }

    function saveResult(word) {
        return $http.post("http://localhost:8081/api/result/", word)
    }


    function saveGameName(newGame) {
        return $http.post("http://localhost:8081/api/game-name/", newGame)
    }

    function saveResultGame(resultGame) {
        return $http.post("http://localhost:8081/api/save-game/", resultGame)
    }
    
    function getResultGame(username, nameGame) {
        return $http.get("http://localhost:8081/api/result-game/" + username + "/" + nameGame)
    }

    return {
        getWords: getWords,
        saveResult: saveResult,
        saveResultOrthography: saveResultOrthography,
        getOrthography: getOrthography,
        saveResult: saveResult,
        saveGameName: saveGameName,
        saveResultGame: saveResultGame,
        getResultGame: getResultGame
    }
});