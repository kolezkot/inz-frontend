angular.module('app')
.service('userService', function($http) {
	
    function login(userCredentials) {
        return $http.post("http://localhost:8081/api/authenticate", userCredentials)
    }

    function register(userData) {
        return $http.post("http://localhost:8081/api/user", userData)
    }

    function saveChilds(childData) {
        return $http.post("http://localhost:8081/api/user-child", childData)
    }

    function getChildren(parent) {
        return $http.post("http://localhost:8081/api/children/", parent)
    }
    
    return {
        login: login,
        register: register,
        saveChilds: saveChilds,
        getChildren: getChildren
    }
});