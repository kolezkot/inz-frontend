angular.module('app')
// Creating the Angular Service for storing logged user details
.service('AuthService', function() {
	return {
		user : localStorage.user ?  JSON.parse(localStorage.user) : null
	}
});