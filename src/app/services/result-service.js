angular.module('app')
.service('resultService', function($http) {

    function getResutlAbc(username) {
        return $http.get("http://localhost:8081/api/result-abc/" + username)
    }

    function getResutlDigits(username) {
        return $http.get("http://localhost:8081/api/result-digit/" + username)
    }

    function getResultOrthography(username) {
        return $http.get("http://localhost:8081/api/result-orthography/" + username)
    }

    return {
        getResutlAbc: getResutlAbc,
        getResutlDigits: getResutlDigits,
        getResultOrthography: getResultOrthography
    }
});