angular
  .module('app')
  .component('parent', {
    templateUrl: 'app/components/parent/parent.html',
    controller: function ($scope, $http, userService, resultService, gameService, AuthService) {

      $scope.loginChildExist = false;

      $scope.getChildren = function () {
        userService.getChildren(AuthService.user)
          .then(function (res) {
            $scope.childs = res.data;
          })
      }
      $scope.getChildren();

      $scope.getResutlAbc = function (username, tab) {
        resultService.getResutlAbc(username)
          .then(function (res) {
            $scope.childAbc = res.data;
            if (res.data.length < 1) {
              $scope.emptyResult = "Brak wyników gry";
            } else {
              delete ($scope.emptyResult);
            }
            $scope.tabResult = tab;
          })
      }

      $scope.getResutlDigits = function (username, tab) {
        resultService.getResutlDigits(username)
          .then(function (res) {
            $scope.childDigit = res.data;
            if (res.data.length < 1) {
              $scope.emptyResult = "Brak wyników gry";
            } else {
              delete ($scope.emptyResult);
            }
            $scope.tabResult = tab;
          })
      }

      $scope.getResultOrthography = function (username, tab) {
        resultService.getResultOrthography(username)
          .then(function (res) {
            $scope.childOrthography = res.data;
            if (res.data.length < 1) {
              $scope.emptyResult = "Brak wyników gry";
            } else {
              delete ($scope.emptyResult);
            }
            $scope.tabResult = tab;
          })
      }

      $scope.getResultGame = function (username, nameGame, tab) {
        gameService.getResultGame(username, nameGame)
          .then(function (res) {
            $scope.childGame = res.data;
              if (res.data.length<1){
                $scope.emptyResult="Brak wyników gry";
                console.log("Brak wyników gry");
              } else {
                delete($scope.emptyResult);
                console.log(res.data);
              }
              $scope.tabResult=tab;
            })
      }

      $scope.getResultGame("c", "moja-gra", 5);


      $scope.viewChild = false;

      $scope.$watchGroup(["dziecko.password", "passwordReplay"], function (newValue) {
        if (newValue[0] === newValue[1]) {
          $scope.formChild.childPassword2.$setValidity("matchChild", true);
          $scope.formChild.childPassword2.$error.matchChild = false;
        } else {
          $scope.formChild.childPassword2.$setValidity("matchChild", false);
          $scope.formChild.childPassword2.$error.matchChild = true;
        }

      });

      $scope.saveChild = function () {
        userService.saveChilds($scope.dziecko)
          .then(function (res) {
            console.log(res);
            $scope.errorRegister = false;
            $scope.viewChild = 0;
            $scope.dziecko = {};
            $scope.formChild.passwordReplay = "";
            $scope.formChild.$setUntouched();
            $scope.tabResult = 0;
            $scope.getChildren();
            $scope.loginChildExist = false;

          }, function (error) {
            // if authentication was not successful. Setting the error message.
            console.log('error register!'); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! alt + shift + f
            $scope.loginChildExist = true;
            $scope.errorRegister = true;
          });
      }

      $scope.viewAddChild = function (tab) {
        // $scope.viewChild = !$scope.viewChild;
        $scope.tabResult = tab;
      }

    },
    controllerAs: "test"
  });