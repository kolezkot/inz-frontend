angular
  .module('app')
  .component('gameEnglish', {
    templateUrl: 'app/components/english/english.html',
    controller: function ($scope, $http, $timeout, userService, gameService) {

      $scope.game = [];
      $scope.resultGame = [{
        idWord: 0,
        good: 0,
        wrong: 0
      }, {}, {}];


      $scope.getWords = function () {
        gameService.getWords()
        .then(function (res) {
          $scope.words = res.data;

          $scope.game[0] = $scope.words[0];
          $scope.game[1] = $scope.words[1];
          $scope.game[2] = $scope.words[2];

          $scope.resultGame[0].idWord = $scope.words[0].id;
          $scope.resultGame[1].idWord = $scope.words[1].id;
          $scope.resultGame[2].idWord = $scope.words[2].id;

        })
      }


      $scope.saveResult = function (word) {
        gameService.saveResult(word)
        .then(function (res) {
          
        })
      }


      $scope.getWords();

      $scope.points = {
        point: [0, 0, 0, 0, 0]
      };


      $scope.add = 3;
      $scope.losowo = Math.floor((Math.random() * 3));


      $scope.check = function (obraz, slowo) {
        if (obraz === slowo) {
          $scope.pasuje = true;
          $scope.resultGame[$scope.losowo].good = 1;
          $scope.resultGame[$scope.losowo].wrong = 0;
          $scope.points.point[$scope.losowo] += 1;


          var timer = $timeout(function () {
            if ($scope.points.point[$scope.losowo] >= 3 && $scope.add < 5) {
              $scope.game[$scope.losowo].englishWord = $scope.words[$scope.add].englishWord;
              $scope.game[$scope.losowo].pathWord = $scope.words[$scope.add].pathWord;
              $scope.resultGame[$scope.losowo].idWord = $scope.words[$scope.add].id;
              $scope.points.point[$scope.losowo] = 0;
              $scope.add = $scope.add + 1;
            }
            $scope.pasuje = false;
            $scope.losowo = Math.floor((Math.random() * 3));
          }, 1300);

        } else {
          $scope.pasuje = false;
          $scope.wrong = obraz;

          var timer = $timeout(function () {
            $scope.wrong = null;
          }, 550);

          if ($scope.points.point[$scope.losowo] >= 0) {
            $scope.resultGame[$scope.losowo].good = 0;
            $scope.resultGame[$scope.losowo].wrong = 1;
            $scope.points.point[$scope.losowo] = $scope.points.point[$scope.losowo] - 1;
          }
        }
        $scope.saveResult($scope.resultGame[$scope.losowo]);
      }
    }
  });