angular
  .module('app')
  .component('log', {
    templateUrl: 'app/components/log/log.html',
    controller: function ($scope, $http, $state, AuthService, $rootScope, userService, $window) {

      $scope.home = function () {
            if (AuthService.user.role==="PARENT") {
                $state.go('homeParent');
              } else if(AuthService.user.role==="CHILD" && AuthService.user.age<5 ) {
                $state.go('homeChild');
              } else if(AuthService.user.role==="CHILD" && AuthService.user.age>=5 ) {
                $state.go('homeSchoolkid');
              }
      }

      $scope.user = AuthService.user;

      $scope.login = function () {
        // requesting the token by usename and passoword
        userService.login($scope.log)
          .then(function (res, event, toState, toParams, fromState, fromParams) {
            $scope.log = {
              password: ''
            }
            if (res.data.token) {
              $scope.message = '';
              // setting the Authorization Bearer token with JWT token
              $http.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.token;

              // setting the user in AuthService
              AuthService.user = res.data.user;
              $rootScope.$broadcast('LoginSuccessful');
              localStorage.user = JSON.stringify(res.data.user); 
              localStorage.token = res.data.token;
              // going to the home page
              $scope.home();
              console.log(res);
              //$state.go('homeParent');
            } else {
              // if the token is not present in the response then the
              // authentication was not successful. Setting the error message.
              $scope.message = 'Authetication Failed 1 !';
            }

          }, function (error) {
            // if authentication was not successful. Setting the error message.
            $scope.message = 'Wprowadzony login lub hasło jest niepoprawne!';
          });
      };
      $scope.$on('LoginSuccessful', function () {
        $scope.user = AuthService.user;
      });
      $scope.$on('LogoutSuccessful', function () {
        $scope.user = null;
      });
      $scope.logout = function () {
        AuthService.user = null;
        localStorage.user = null;
        localStorage.token = null;
        $rootScope.$broadcast('LogoutSuccessful');
        $state.go('app');
      };
      // $scope.backUp = function () {
      //   //$state.go('app');
      //   $window.history.back();
      //   console.log("wroc");
      // };
    }
  });
