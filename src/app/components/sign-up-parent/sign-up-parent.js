angular
  .module('app')
  .component('signUpParent', {
    templateUrl: 'app/components/sign-up-parent/sign-up-parent.html',
    controller: function($scope, $http, userService) {

      $scope.loginExist=false;
      $scope.$watch('parent.username', function () {
        $scope.loginExist=false;
      });
      
      $scope.$watchGroup(["parent.password" ,"replayPassword"], function (newValue) {
        if (newValue[0] === newValue[1]) {
            $scope.form.replayPassword.$setValidity("match", true);
            $scope.form.replayPassword.$error.match = false;
        } else {
            $scope.form.replayPassword.$setValidity("match", false);
            $scope.form.replayPassword.$error.match = true;
        }
        });

    $scope.check = function () {
        userService.register($scope.parent)
        .then(function (res) {
          $scope.sendRegister=true;   
          $scope.errorRegister=false;
        }, function (error) {
            $scope.loginExist=true;
            $scope.sendRegister=false;                      
          });
      }
    }

  });