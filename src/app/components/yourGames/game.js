angular
    .module('app')
    .component('yourGame', {
        templateUrl: 'app/components/yourGames/game.html',
        controller: function ($scope, $http, $state, gameService) {

            $scope.newGame = { nameGame: "moja gra" };

            $scope.saveGameName = function () {
                gameService.saveGameName($scope.newGame)
                    .then(function (res) {
                        console.log("zapisano nazwe gry: " + res);
                        console.log(res.data);
                    }, function (error) {
                        console.log('taka nazwa gry już istnieje!');
                    })
            }
            //$scope.saveGameName();


            $scope.resultGame = {
                nameGame: "moja gra",
                point: null,     // or ather value of number
                name: "quiz",      // name word or level
                isAright: true,    //true or false
            };

            $scope.saveResultGame = function () {
                gameService.saveResultGame($scope.resultGame)
                .then(function (res) {
                    console.log("zapisano wyniki gry: " + res);
                    console.log(res.data);
                }, function (error) {
                    console.log('gra o takiej nazwie nie isnieje');
                })
            }
            //$scope.saveResultGame();
        }
    });