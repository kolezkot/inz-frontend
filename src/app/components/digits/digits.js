angular
    .module('app')
    .component('gameDigits', {
        templateUrl: 'app/components/digits/digits.html',
        controller: function ($scope, $http, $timeout, $state, $window) {

            $scope.resultDigit = {
                digit: null,
                isGood: null
            };

            $scope.saveResultDigit = function () {
                $http.post("http://localhost:8081/api/result-digit/", $scope.resultDigit).then(function (res) {
                    console.log(res);
                    console.log("wyslalodogitsy");
                    console.log(res.data);
                })
            }

            $scope.stop = function () {
                myGameArea.stop();
                return;
            }
            $scope.add = 1;
            var box;
            var myObstacles = [];

            function startGame() {
                box = new component(45, 45, "img/digits/dice" + $scope.add + ".png", 220, 280, "image", "digit" + $scope.add);
                myBackground = new component(500, 550, "img/digits/deski.jpg", 100, 0, "image");
                myUpBtn = new component(50, 50, "img/arrow.png", 15, 180, "image");
                myDownBtn = new component(50, 50, "img/arrowdown.png", 15, 290, "image");
                myLeftBtn = new component(50, 50, "img/arrowleft.png", 630, 220, "image");
                myRightBtn = new component(50, 50, "img/arrowright.png", 752, 220, "image");
                myGameArea.start();
            }

            var myGameArea = {
                canvas: document.createElement("canvas"),
                start: function () {
                    this.canvas.width = 800;
                    this.canvas.height = 550;
                    this.context = this.canvas.getContext("2d");
                    div = document.getElementById("start");
                    div.appendChild(this.canvas);
                    //document.body.insertBefore(this.canvas, document.body.childNodes[0]);
                    this.interval = setInterval(updateGameArea, 20);
                    this.frameNo = 0;
                    window.addEventListener('keydown', function (e) {
                        myGameArea.keys = (myGameArea.keys || []);
                        myGameArea.keys[e.keyCode] = true;
                    })
                    window.addEventListener('keyup', function (e) {
                        myGameArea.keys[e.keyCode] = false;
                    })
                    window.addEventListener('mousedown', function (e) {
                        myGameArea.x = e.pageX;
                        myGameArea.y = e.pageY;
                        //console.log(e.pageX);
                    })
                    window.addEventListener('mouseup', function (e) {
                        myGameArea.x = false;
                        myGameArea.y = false;
                    })
                    div.addEventListener('touchstart', function (e) {
                        myGameArea.x = (e.changedTouches[0].pageX - e.changedTouches[0].target.offsetLeft) * myGameArea.canvas.width / myGameArea.canvas.clientWidth | 0;
                        myGameArea.y = (e.changedTouches[0].pageY - e.changedTouches[0].target.offsetTop) * myGameArea.canvas.height / myGameArea.canvas.clientHeight | 0;
                        console.log((e.changedTouches[0].pageX - e.changedTouches[0].target.offsetLeft) * myGameArea.canvas.width / myGameArea.canvas.clientWidth | 0);
                        console.log(myGameArea.canvas.clientWidth);
                    })
                    div.addEventListener('touchend', function (e) {
                        myGameArea.x = false;
                        myGameArea.y = false;
                        //console.log(e.changedTouches[0].pageY - e.changedTouches[0].target.offsetTop);
                    })
                },
                clear: function () {
                    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
                },
                stop: function () {
                    clearInterval(this.interval);
                }
            }

            function component(width, height, color, x, y, type, name) {
                this.width = width;
                this.height = height;
                this.speedX = 0;
                this.speedY = 0;
                this.x = x;
                this.y = y;
                this.type = type;
                if (type == "image") {
                    this.image = new Image();
                    this.image.src = color;
                }
                this.name = name;
                this.update = function () {
                    ctx = myGameArea.context;
                    if (type == "image") {
                        ctx.drawImage(this.image,
                            this.x,
                            this.y,
                            this.width, this.height);
                    } else {
                        ctx.fillStyle = color;
                        ctx.fillRect(this.x, this.y, this.width, this.height);
                    }
                }
                this.clicked = function () {
                    var myleft = this.x;
                    var myright = this.x + (this.width);
                    var mytop = this.y;
                    var mybottom = this.y + (this.height);
                    var clicked = true;
                    if ((mybottom < myGameArea.y) ||
                        (mytop > myGameArea.y) ||
                        (myright < myGameArea.x) ||
                        (myleft > myGameArea.x)) {
                        clicked = false;
                    }
                    return clicked;
                }
                this.newPos = function () {
                    this.x += this.speedX;
                    this.y += this.speedY;
                }
                this.crashWith = function (otherobj) {
                    var myleft = this.x;
                    var myright = this.x + (this.width);
                    var mytop = this.y;
                    var mybottom = this.y + (this.height);
                    var otherleft = otherobj.x;
                    var otherright = otherobj.x + (otherobj.width);
                    var othertop = otherobj.y;
                    var otherbottom = otherobj.y + (otherobj.height);
                    var crash = true;
                    if ((mybottom < othertop) ||
                        (mytop > otherbottom) ||
                        (myright < otherleft) ||
                        (myleft > otherright)) {
                        crash = false;
                    }
                    return crash;
                }
            }

            function updateGameArea() {
                if (!$state.includes("digits")) {
                    $scope.stop();
                };
                if ($scope.change) {
                    setTimeout(animate, 500);
                }

                $scope.losowo = Math.floor((Math.random() * 6) + 1);

                var x, y;
                for (i = 0; i < myObstacles.length; i += 1) {
                    if (box.crashWith(myObstacles[i])) {
                        console.log("aaaaa walnąłem!!!");
                        //console.log(myObstacles[i]);

                        console.log(myObstacles[i].name);
                        myObstacles[i].width = null;
                        myObstacles[i].height = null;
                        myObstacles[i].x = null;
                        myObstacles[i].x = null;

                        if (myObstacles[i].name !== box.name) {
                            console.log("blad");
                            box.image.src = "img/digits/wrong" + $scope.add + ".png";
                            $scope.resultDigit = {
                                digit: $scope.add,
                                isGood: false
                            };
                            $scope.saveResultDigit();
                            $scope.change = true;
                        }
                        if (myObstacles[i].name === box.name) {
                            if ($scope.add === 6) {
                                $scope.add = 0;
                            }
                            $scope.resultDigit = {
                                digit: $scope.add,
                                isGood: true
                            };
                            $scope.saveResultDigit();
                            $scope.add += 1;
                            console.log("dobrze!!!");
                            box.image.src = "img/digits/dice" + $scope.add + ".png";
                            box.name = "digit" + $scope.add;
                        }
                        //myGameArea.stop();
                        //   return;
                    }
                }
                // $timeout(function(){
                //     box.image.src = "img/digits/six.png";
                // },1000);
                myGameArea.clear();
                box.speedX = 0;
                box.speedY = 0;
                if (myGameArea.x && myGameArea.y) {
                    if (myUpBtn.clicked()) {
                        console.log("up up up");
                        box.speedY = -1;
                    }
                    if (myDownBtn.clicked()) {
                        box.speedY = 1;
                    }
                    if (myLeftBtn.clicked()) {
                        box.speedX = -1;
                    }
                    if (myRightBtn.clicked()) {
                        box.speedX = 1;
                    }
                }
                myUpBtn.update();
                myDownBtn.update();
                myLeftBtn.update();
                myRightBtn.update();
                //myBackground.newPos();
                myBackground.update();
                myGameArea.frameNo += 1;
                if (myGameArea.frameNo == 1 || everyinterval(70)) {
                    $scope.placeX = Math.floor((Math.random() * 400) + 240);
                    x = myGameArea.canvas.width - $scope.placeX;
                    y = myGameArea.canvas.height - 560;
                    myObstacles.push(new component(30, 30, "img/digits/digit" + $scope.losowo + ".png", x, y, "image", "digit" + $scope.losowo));
                }
                for (i = 0; i < myObstacles.length; i += 1) {
                    myObstacles[i].y += 1;
                    myObstacles[i].update();
                }
                if (myGameArea.keys && myGameArea.keys[37]) { box.speedX = -1; }
                if (myGameArea.keys && myGameArea.keys[39]) { box.speedX = 1; }
                if (myGameArea.keys && myGameArea.keys[38]) { box.speedY = -1; }
                if (myGameArea.keys && myGameArea.keys[40]) { box.speedY = 1; }
                box.newPos();
                box.update();
            }

            function animate() {
                console.log("czekaj");
                box.image.src = "img/digits/dice" + $scope.add + ".png";
                $scope.change = false;
            }

            function everyinterval(n) {
                if ((myGameArea.frameNo / n) % 1 == 0) { return true; }
                return false;
            }

            $scope.moveup = function () {
                console.log("czekaj");
                box.speedY = -1;
            }

            $scope.movedown = function () {
                box.speedY = 1;
            }

            $scope.moveleft = function () {
                box.speedX = -1;
            }

            $scope.moveright = function () {
                box.speedX = 1;
            }

            $scope.clearmove = function () {
                box.speedX = 0;
                box.speedY = 0;
            }

            startGame();

        }
    });