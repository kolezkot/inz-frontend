angular
    .module('app')
    .component('gameOrthography', {
        templateUrl: 'app/components/orthography/orthography.html',
        controller: function ($scope, $http, $timeout, $state, $window, userService, gameService) {

            $scope.resultOrthography = {
                idOrthography: 0,
                good: 0,
                wrong: 0
            };


            var letters = [];
            var myGamePiece;
            $scope.myObstacles = [];
            $scope.goodWord = [];
            var letter = [];
            $scope.nr = 0;
            $scope.save = false;
            var ground;

            $scope.saveResultOrthography = function () {
                gameService.saveResultOrthography($scope.resultOrthography)
                    .then(function (res) {
                        console.log(res);
                        console.log("wyslalo");
                        console.log(res.data);
                    })
            }


            $scope.getOrthography = function () {
                gameService.getOrthography()
                    .then(function (res) {
                        $scope.orthography = res.data;
                        console.log($scope.orthography);
                        $scope.word = Math.floor((Math.random() * $scope.orthography.length));
                        console.log($scope.word);
                        //var str = $scope.orthography[$scope.word].word;
                        //console.log(str);
                        $scope.showWord = $scope.orthography[$scope.word];
                        $scope.goodWord = $scope.orthography[$scope.word].word.split("");
                        var litery = $scope.orthography[$scope.word].word + $scope.orthography[$scope.word].wrongLetter;
                        console.log(litery);
                        letters = litery.split("");
                        console.log(letters);
                        console.log(letters[0]);
                        for (i = 0; i < letters.length; i += 1) {
                            var losowoMyObstacles = Math.floor((Math.random() * $scope.myObstacles.length));
                            console.log("losowoMyObstacles " + losowoMyObstacles);
                            var losowoX = Math.floor((Math.random() * ($scope.myObstacles[losowoMyObstacles].width - 30)) + ($scope.myObstacles[losowoMyObstacles].x));
                            var losowoY = ($scope.myObstacles[losowoMyObstacles].y) - 30;
                            letter[i] = new component(30, 30, "img/orthography/" + letters[i] + ".png", losowoX, losowoY, "image");
                        }
                        console.log("$scope.showLettera " + $scope.showLettera);
                    })
            }

            $scope.showLetters = function (number) {
                $scope.showLettera = $scope.showWord.word.substring(0, number);
                console.log("$scope.showLettera " + $scope.showLettera);
            }



            $scope.stop = function () {
                myGameArea.stop();
                return;
            }


            function startGame() {
                myGamePiece = new component(30, 30, "img/orthography/clementine.png", 10, 120, "image", 0.05);
                myBackground = new component(480, 480, "img/orthography/niebo.png", 0, 0, "image");
                $scope.myObstacles[0] = new component(120, 15, "img/orthography/earth.png", 350, 90, "image");
                $scope.myObstacles[1] = new component(220, 20, "img/orthography/earth.png", 270, 150, "image");
                $scope.myObstacles[2] = new component(320, 15, "img/orthography/earth.png", 130, 219, "image");
                $scope.myObstacles[3] = new component(420, 20, "img/orthography/earth.png", 38, 280, "image");
                $scope.myObstacles[4] = new component(100, 20, "img/orthography/earth.png", 0, 340, "image");
                $scope.myObstacles[5] = new component(200, 40, "img/orthography/earth.png", 0, 400, "image");
                $scope.myObstacles[6] = new component(100, 10, "img/orthography/earth.png", 250, 444, "image");
                myUpBtn = new component(40, 40, "img/arrow.png", 10, 10, "image");
                myDownBtn = new component(40, 40, "img/space2.png", 10, 60, "image");
                myLeftBtn = new component(40, 40, "img/arrowleft.png", 380, 420, "image");
                myRightBtn = new component(40, 40, "img/arrowright.png", 440, 420, "image");

                //myObstacle  = new component(10, 200, "img/orthography/earth.png", 300, 120);    
                myGameArea.start();

            }

            var myGameArea = {
                canvas: document.createElement("canvas"),
                start: function () {
                    this.canvas.width = 480;
                    this.canvas.height = 480;
                    this.context = this.canvas.getContext("2d");
                    div = document.getElementById("orthography");
                    div.appendChild(this.canvas);
                    this.interval = setInterval(updateGameArea, 20);
                    window.addEventListener('keydown', function (e) {
                        myGameArea.keys = (myGameArea.keys || []);
                        myGameArea.keys[e.keyCode] = true;
                    })
                    window.addEventListener('keyup', function (e) {
                        myGameArea.keys[e.keyCode] = false;
                    })
                    div.addEventListener('touchstart', function (e) {
                        myGameArea.x = (e.changedTouches[0].pageX - e.changedTouches[0].target.offsetLeft)
                            * myGameArea.canvas.width / myGameArea.canvas.clientWidth | 0;
                        myGameArea.y = (e.changedTouches[0].pageY - e.changedTouches[0].target.offsetTop)
                            * myGameArea.canvas.height / myGameArea.canvas.clientHeight | 0;
                    })
                    div.addEventListener('touchend', function (e) {
                        myGameArea.x = false;
                        myGameArea.y = false;
                    })
                },
                clear: function () {
                    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
                },
                stop: function () {
                    clearInterval(this.interval);
                }
            }

            function component(width, height, color, x, y, type, gravity) {
                this.width = width;
                this.height = height;
                this.speedX = 0;
                this.speedY = 0;
                this.x = x;
                this.y = y;
                this.type = type;
                if (type == "image") {
                    this.image = new Image();
                    this.image.src = color;
                }
                this.gravity = gravity;
                this.gravitySpeed = 0;
                this.update = function () {
                    ctx = myGameArea.context;
                    if (type == "image") {
                        ctx.drawImage(this.image,
                            this.x,
                            this.y,
                            this.width, this.height);
                    } else {
                        ctx.fillStyle = color;
                        ctx.fillRect(this.x, this.y, this.width, this.height);
                    }
                }
                this.clicked = function () {
                    var myleft = this.x;
                    var myright = this.x + (this.width);
                    var mytop = this.y;
                    var mybottom = this.y + (this.height);
                    var clicked = true;
                    if ((mybottom < myGameArea.y) ||
                        (mytop > myGameArea.y) ||
                        (myright < myGameArea.x) ||
                        (myleft > myGameArea.x)) {
                        clicked = false;
                    }
                    return clicked;
                }
                this.newPos = function () {
                    this.gravitySpeed += this.gravity;
                    this.x += this.speedX;
                    this.y += this.speedY + this.gravitySpeed;
                    this.hitBottom();
                    this.hitRight();
                    this.hitLeft();
                }
                this.hitBottom = function () {
                    var rockbottom = myGameArea.canvas.height - this.height;
                    if (this.y > rockbottom) {
                        this.y = rockbottom;
                        this.gravitySpeed = 0;
                        ground = true;
                    } else {
                        ground = false;
                    }

                }

                this.hitRight = function () {
                    var rockright = myGameArea.canvas.width - this.width;
                    if (this.x > rockright) {
                        this.x = rockright;

                    } else {


                    }

                }
                this.hitLeft = function () {
                    var rockleft = myGameArea.canvas.width - myGameArea.canvas.width;
                    if (this.x < rockleft) {
                        this.x = rockleft;

                    } else {

                    }

                }

                this.crashWith = function (otherobj) {
                    var myMiddleX = this.x + this.width / 2 + myGamePiece.speedX;
                    var myMiddleY = this.y + this.height / 2 + myGamePiece.speedY;

                    var otherleft = otherobj.x - this.width / 2;
                    var otherright = otherobj.x + (otherobj.width) + this.width / 2;
                    var othertop = otherobj.y - this.height / 2;
                    var otherbottom = otherobj.y + (otherobj.height) + this.height / 2;

                    var crash = false;
                    noStep = false;
                    if ((myMiddleX > otherleft) &&
                        (myMiddleX < otherright) &&
                        (myMiddleY > othertop) &&
                        (myMiddleY < otherbottom)
                    ) {
                        crash = true;
                    }
                    if (myMiddleY >= othertop &&
                        myMiddleY < otherbottom) {
                        noStep = true;
                    }
                    return crash;
                }

                this.crashWithY = function (otherobj) {
                    var myMiddleX = this.x + this.width / 2 + myGamePiece.speedX;
                    var myMiddleY = this.y + this.height / 2 + myGamePiece.speedY;

                    var otherleft = otherobj.x - this.width / 2;
                    var otherright = otherobj.x + (otherobj.width) + this.width / 2;
                    var othertop = otherobj.y - this.height / 2;
                    var otherbottom = otherobj.y + (otherobj.height) + this.height / 2;

                    var myMiddleXOther = otherobj.x + otherobj.width / 2;
                    var myMiddleYOther = otherobj.y + otherobj.height / 2;

                    var crashY = false;
                    if ((myMiddleY < myMiddleYOther) &&
                        (myMiddleY + 2 > othertop - 2) &&
                        (myMiddleX > otherleft) &&
                        (myMiddleX < otherright)
                    ) {
                        ground = true;
                        crashY = true;
                    }
                    return crashY;
                }

                this.crashWithYbot = function (otherobj) {
                    var myMiddleX = this.x + this.width / 2 + myGamePiece.speedX;
                    var myMiddleY = this.y + this.height / 2 + myGamePiece.speedY;

                    var otherleft = otherobj.x - this.width / 2;
                    var otherright = otherobj.x + (otherobj.width) + this.width / 2;
                    var othertop = otherobj.y - this.height / 2;
                    var otherbottom = otherobj.y + (otherobj.height) + this.height / 2;

                    var myMiddleXOther = otherobj.x + otherobj.width / 2;
                    var myMiddleYOther = otherobj.y + otherobj.height / 2;

                    var crashYbot = false;
                    if ((myMiddleY > myMiddleYOther) &&
                        (myMiddleY + 2 < otherbottom) &&
                        (myMiddleX > otherleft) &&
                        (myMiddleX < otherright)
                    ) {
                        crashYbot = true;
                    }
                    return crashYbot;
                }
            }


            function updateGameArea() {
                if (!$state.includes("orthography")) {
                    $scope.stop();
                };
                myGameArea.clear();
                myBackground.update();

                if ((myGameArea.keys && myGameArea.keys[37]) || (myGameArea.x && myGameArea.y && (myLeftBtn.clicked()))) {
                    $scope.save = true;
                    myGamePiece.speedX = -2;
                    for (i = 0; i < $scope.myObstacles.length; i += 1) {
                        if (myGamePiece.crashWith($scope.myObstacles[i])) {
                            myGamePiece.speedX = 0;
                        }
                    }
                }
                if ((myGameArea.keys && myGameArea.keys[39]) || (myGameArea.x && myGameArea.y && (myRightBtn.clicked()))) {
                    $scope.save = true;
                    myGamePiece.speedX = 2;
                    for (i = 0; i < $scope.myObstacles.length; i += 1) {
                        if (myGamePiece.crashWith($scope.myObstacles[i])) {
                            myGamePiece.speedX = 0;
                        }
                    }
                }


                for (i = 0; i < $scope.myObstacles.length; i += 1) {
                    if (myGamePiece.crashWithY($scope.myObstacles[i])) {
                        ground = true;
                        myGamePiece.speedY = 0;
                        myGamePiece.gravity = 0;
                        myGamePiece.gravitySpeed = 0;
                    }
                }

                if ((myGameArea.keys && myGameArea.keys[38] && ground) || (myGameArea.x && myGameArea.y && (myUpBtn.clicked()) && ground)) {
                    accelerate(-4.5); ground = false; $scope.save = true;
                }

                if (myGameArea.keys && myGameArea.keys[40]) {
                    myGamePiece.speedY = 1;
                    $scope.save = true;
                    for (i = 0; i < $scope.myObstacles.length; i += 1) {
                        if (myGamePiece.crashWithY($scope.myObstacles[i])) {
                            myGamePiece.speedY = 0;
                            myGamePiece.gravity = 0;
                            myGamePiece.gravitySpeed = 0;
                        }
                    }
                }

                if ((myGameArea.keys && myGameArea.keys[32] && $scope.save) ||
                    (myGameArea.x && myGameArea.y && (myDownBtn.clicked()) && $scope.save)) {
                    for (i = 0; i < letter.length; i += 1) {
                        if (myGamePiece.crashWith(letter[i])) {
                            if ($scope.nr === letter.length) {
                                $scope.nr = 0;
                            }
                            if (letters[i] === $scope.goodWord[$scope.nr]) {
                                $scope.nr += 1;
                                myBackground.image.src = "img/orthography/niebogood.png";
                                $scope.resultOrthography.idOrthography = $scope.showWord.id;
                                $scope.resultOrthography.good = 1;
                                $scope.resultOrthography.wrong = 0;
                                $scope.saveResultOrthography();
                                letter[i].width = 0;
                                letter[i].height = 0;
                                letter[i].x = -990;
                                letter[i].y = -990;
                                $scope.showLetters($scope.nr);
                                $scope.change = true;
                            } else {
                                myGamePiece.image.src = "img/orthography/clementineWrong.png";
                                myBackground.image.src = "img/orthography/nieboWrong.png";
                                $scope.resultOrthography.idOrthography = $scope.showWord.id;
                                $scope.resultOrthography.good = 0;
                                $scope.resultOrthography.wrong = 1;
                                $scope.saveResultOrthography();
                                $scope.change = true;
                                $scope.save = false;
                            }
                        }
                    }
                }
                if ($scope.change) {
                    setTimeout(animate, 700);
                }

                for (i = 0; i < $scope.myObstacles.length; i += 1) {
                    if (myGamePiece.crashWithYbot($scope.myObstacles[i])) {

                        myGamePiece.gravitySpeed = +1;
                    }
                }

                for (i = 0; i < $scope.myObstacles.length; i += 1) {
                    $scope.myObstacles[i].update();
                }
                for (i = 0; i < letter.length; i += 1) {
                    letter[i].update();
                }

                myGamePiece.newPos();
                myGamePiece.update();
                myGamePiece.speedX = 0;
                myGamePiece.speedY = 0;
                myUpBtn.update();
                myDownBtn.update();
                myLeftBtn.update();
                myRightBtn.update();
                console.log("$scope.showLettera " + $scope.showLettera + "  abcd");
                $scope.mariola = "matttttro";
                accelerate(0.15);

            }

            function accelerate(n) {
                myGamePiece.gravity = n;
            }

            function everyinterval(n) {
                if ((myGameArea.frameNo / n) % 1 == 0) { return true; }
                return false;
            }

            function animate() {
                console.log("czekaj");
                myGamePiece.image.src = "img/orthography/clementine.png";
                myBackground.image.src = "img/orthography/niebo.png";
                $scope.change = false;
            }

            startGame();
            $scope.getOrthography();

        }


    });