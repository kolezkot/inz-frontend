angular
  .module('app')
  .config(routesConfig);

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('app', {
      url: '/',
      component: 'signUpParent'
    })
    .state('homeChild', {
      url: '/home-child',
      component: 'preschooler',
      data: {
        authorization: true,
        redirectTo: 'app',
        role: "child"
      }
    })
    .state('homeParent', {
      url: '/home-parent',
      component: 'parent',
      data: {
        authorization: true,
        redirectTo: 'app',
        role: "parent"
      }
    })
    .state('abc', {
      url: '/home-child/abc',
      component: 'gameGlobalread',
      data: {
        authorization: true,
        redirectTo: 'app',
        role: "child"
      }
    })
    .state('digits', {
      url: '/home-child/digits',
      component: 'gameDigits',
      data: {
        authorization: true,
        redirectTo: 'app',
        role: "child"
      }
    })
    .state('talePreschooler', {
      url: '/home-child/tale',
      component: 'talePreschooler',
      data: {
        authorization: true,
        redirectTo: 'app',
        role: "child"
      }
    })
    .state('homeSchoolkid', {
      url: '/home-schoolkid',
      component: 'schoolkid',
      data: {
        authorization: true,
        redirectTo: 'app',
        role: "child"
      }
    })
    .state('orthography', {
      url: '/home-schoolkid/orthography',
      component: 'gameOrthography',
      data: {
        authorization: true,
        redirectTo: 'app',
        role: "child"
      }
    })
    .state('english', {
      url: '/home-schoolkid/english',
      component: 'gameEnglish',
      data: {
        authorization: true,
        redirectTo: 'app',
        role: "child"
      }
    })
    .state('taleSchoolkid', {
      url: '/home-schoolkid/tale',
      component: 'taleSchoolkid',
      data: {
        authorization: true,
        redirectTo: 'app',
        role: "child"
      }
    })
    .state('yourGame', {
      url: '/game',
      component: 'yourGame',
      data: {
        authorization: true,
        redirectTo: 'app',
        role: "child"
      }
    })
    ;
}
