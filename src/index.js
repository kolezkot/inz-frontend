angular
  .module('app', ['ui.router'])
  .run(function (AuthService, $transitions, $rootScope, $http) {
    $transitions.onStart({ to: '*' }, function (trans, $scope) {
      console.log(localStorage);
      if (AuthService.user) {
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.token;
      }
      if (!AuthService.user && trans.$to().data && trans.$to().data.authorization) {
        // User isn't authenticated. Redirect to a new Target State
        console.log("nie masz uprawnień");
        return trans.router.stateService.target('app');
      }
      //console.log(trans.$to().component);
      // if (AuthService.user && trans.$to().data && trans.$to().data.authorization) {
      //   return trans.router.stateService.target('app');
      // }
      //console.log(trans.$to().data.role!=AuthService.role);
    });
  });

